package main

import (
	"errors"
	"fmt"
	"fuel/apollo/apollo"
	"fuel/apollo/postgres"
	"fuel/gu-apollo/web/model"
	"log"

	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
)

var (
	db *sqlx.DB
)

func getCard(query apollo.Query) (interface{}, error) {

	id, err := query.Int64("id")
	if err != nil {
		return nil, err
	}

	var card model.Card

	err = db.Get(&card, `SELECT * FROM card WHERE "id" = $1;`, id)

	if err != nil {
		return nil, err
	}

	return card, nil
}

func getProto(query apollo.Query) (interface{}, error) {

	id, err := query.Int64("id")
	if err != nil {
		return nil, err
	}

	var cards []model.Card

	err = db.Select(&cards, `SELECT * FROM card WHERE proto = $1;`, id)

	if err != nil {
		return nil, err
	}

	return cards, nil
}

func getCards(query apollo.Query) (interface{}, error) {

	var cards []model.Card

	err := db.Select(&cards, `SELECT * FROM card;`)

	if err != nil {
		return nil, err
	}

	return cards, nil
}

func getAuctions(query apollo.Query) (interface{}, error) {

	var auctions []model.Auction

	err := db.Select(&auctions, `SELECT * FROM auction;`)

	if err != nil {
		return nil, err
	}

	return auctions, nil
}

func getAuction(query apollo.Query) (interface{}, error) {

	id, err := query.Int64("id")
	if err != nil {
		return nil, err
	}

	var auction model.Auction

	err = db.Get(&auction, `SELECT * FROM auction WHERE id = $1;`, id)

	if err != nil {
		return nil, err
	}

	return auction, nil
}

func getUserOpenedCards(query apollo.Query) (interface{}, error) {

	address, err := query.String("address")

	if err != nil {
		return nil, err
	}

	var cards []model.Card

	err = db.Select(&cards, `SELECT * FROM card WHERE LOWER("user") = LOWER($1) AND opened = true;`, address)

	if err != nil {
		return nil, err
	}

	return cards, nil
}

func getUserCards(query apollo.Query) (interface{}, error) {

	address, err := query.String("address")

	if err != nil {
		return nil, err
	}

	var cards []model.Card

	err = db.Select(&cards, `SELECT * FROM card WHERE LOWER("user") = LOWER($1);`, address)

	if err != nil {
		return nil, err
	}

	return cards, nil
}

func getPurchase(query apollo.Query) (interface{}, error) {

	id, err := query.Int64("id")
	if err != nil {
		return nil, err
	}

	var purchase model.Purchase

	err = db.Select(&purchase, `SELECT * FROM purchase WHERE LOWER("id") = LOWER($1);`, id)

	if err != nil {
		return nil, err
	}

	return purchase, nil
}

func getBids(query apollo.Query) (interface{}, error) {

	id, err := query.Int64("id")
	if err != nil {
		return nil, err
	}

	var purchase model.Purchase

	err = db.Select(&purchase, `SELECT * FROM bid WHERE auctionid = $1;`, id)

	if err != nil {
		return nil, err
	}

	return purchase, nil
}

func getPurchases(query apollo.Query) (interface{}, error) {

	var purchases []model.Purchase

	err := db.Select(&purchases, `SELECT * FROM purchase;`)

	if err != nil {
		return nil, err
	}

	return purchases, nil
}

func getUnclaimedUserPurchases(query apollo.Query) (interface{}, error) {

	address, err := query.String("address")

	if err != nil {
		return nil, err
	}

	var purchases []model.Purchase

	err = db.Select(&purchases, `SELECT * FROM purchase WHERE LOWER("user")=LOWER($1) AND remaining > 0;`, address)

	if err != nil {
		return nil, err
	}

	return purchases, nil
}

func getUserPurchases(query apollo.Query) (interface{}, error) {

	address, err := query.String("address")

	if err != nil {
		return nil, err
	}

	var purchases []model.Purchase

	err = db.Select(&purchases, `SELECT * FROM purchase WHERE LOWER("user")=LOWER($1);`, address)

	if err != nil {
		return nil, err
	}

	return purchases, nil
}

func getReferrals(query apollo.Query) (interface{}, error) {

	address, err := query.String("address")
	if err != nil {
		return nil, err
	}

	var referrals []model.Referral

	err = db.Select(&referrals, `SELECT * FROM referral WHERE LOWER("referrer")=LOWER($1);`, address)

	if err != nil {
		return nil, err
	}

	return referrals, nil
}

func fill(id string, indices pq.Int64Array) ([]model.Card, error) {

	base := fmt.Sprintf(`SELECT * FROM card WHERE queryid = '%s' AND queryindex IN (?);`, id)

	query, args, err := sqlx.In(base, indices)

	if err != nil {
		return nil, err
	}

	var cards []model.Card

	query = db.Rebind(query)
	err = db.Select(&cards, query, args...)
	if err != nil {
		return nil, err
	}
	return cards, nil
}

func getPacks(query apollo.Query) (interface{}, error) {

	var packs []*model.Pack

	err := db.Select(&packs, `SELECT * FROM pack;`)

	if err != nil {
		return nil, err
	}

	for _, p := range packs {
		p.Cards, err = fill(p.QueryID, p.QueryIndices)
		if err != nil {
			return nil, err
		}
	}

	if err != nil {
		return nil, err
	}

	return packs, nil
}

func getUserPacks(query apollo.Query) (interface{}, error) {

	address, err := query.String("address")
	if err != nil {
		return nil, err
	}

	var packs []*model.Pack

	err = db.Select(&packs, `SELECT * FROM pack WHERE LOWER("user")=LOWER($1);`, address)

	if err != nil {
		return nil, err
	}

	for _, p := range packs {
		p.Cards, err = fill(p.QueryID, p.QueryIndices)
		if err != nil {
			return nil, err
		}
	}

	if err != nil {
		return nil, err
	}

	return packs, nil
}

func getUnopenedPacks(query apollo.Query) (interface{}, error) {

	address, err := query.String("address")
	if err != nil {
		return nil, err
	}

	var packs []*model.Pack

	err = db.Select(&packs, `SELECT * FROM pack WHERE LOWER("user")=LOWER($1) AND opened = false;`, address)

	if err != nil {
		return nil, err
	}

	for _, p := range packs {
		p.Cards, err = fill(p.QueryID, p.QueryIndices)
		if err != nil {
			return nil, err
		}
	}

	if err != nil {
		return nil, err
	}

	return packs, nil
}

func getPack(query apollo.Query) (interface{}, error) {

	id, err := query.Int64("id")
	if err != nil {
		return nil, err
	}

	var pack model.Pack

	err = db.Get(&pack, `SELECT * FROM pack WHERE "id" = $1`, id)

	if err != nil {
		return nil, err
	}

	return pack, nil
}

func getUser(query apollo.Query) (interface{}, error) {

	address, err := query.String("address")
	if err != nil {
		return nil, err
	}

	var user model.User

	err = db.Get(&user, `SELECT * FROM guuser WHERE LOWER(address)=LOWER($1)`, address)

	if err != nil {
		return nil, err
	}

	return user, nil
}

func getUserByName(query apollo.Query) (interface{}, error) {

	name, err := query.String("name")
	if err != nil {
		return nil, err
	}

	var user model.User

	err = db.Get(&user, `SELECT * FROM guuser WHERE LOWER(name)=LOWER($1)`, name)

	if err != nil {
		return nil, err
	}

	return user, nil
}

func openPack(query apollo.Query) (interface{}, error) {

	id, err := query.String("id")

	if err != nil {
		return nil, err
	}

	index, err := query.Int64("index")

	if err != nil {
		return nil, err
	}

	log.Println("params:", id, index)

	var p model.Pack

	err = db.Get(&p, `UPDATE pack SET opened = true WHERE queryid = $1 AND queryindex = $2 RETURNING *;`, id, index)

	if err != nil {
		return nil, err
	}

	base := fmt.Sprintf(`UPDATE card SET opened = true WHERE queryid = '%s' AND queryindex IN (?);`, p.QueryID)

	q, args, err := sqlx.In(base, p.QueryIndices)

	if err != nil {
		return nil, err
	}

	q = db.Rebind(q)

	_, err = db.Query(q, args...)
	if err != nil {
		return nil, err
	}

	return p, nil
}

func createUser(query apollo.Query) (interface{}, error) {

	val, ok := query.Params["user"]

	if !ok {
		return nil, errors.New("missing param")
	}

	user := val.(model.User)

	err := db.Get(&user, `INSERT INTO guuser (address, email, username, nonce) VALUES ($1, $2, $3, 0);`, user.Address, user.Email, user.Username)

	if err != nil {
		return nil, err
	}

	return user, nil
}

func updateUser(query apollo.Query) (interface{}, error) {

	val, ok := query.Params["user"]

	if !ok {
		return nil, errors.New("missing param")
	}

	user := val.(model.User)

	err := db.Get(&user, `INSERT INTO guuser (address, email, username, nonce) VALUES ($1, $2, $3, nonce + 1);`, user.Address, user.Email, user.Username)

	if err != nil {
		return nil, err
	}

	return user, nil
}

var getters = postgres.QueryMap{
	"Proto":                  postgres.Query(getProto),
	"Card":                   postgres.Query(getCard),
	"Cards":                  postgres.Query(getCards),
	"UserCards":              postgres.Query(getUserCards),
	"UserOpenedCards":        postgres.Query(getUserOpenedCards),
	"Pack":                   postgres.Query(getPack),
	"Packs":                  postgres.Query(getPacks),
	"UserPacks":              postgres.Query(getUserPacks),
	"UnopenedPacks":          postgres.Query(getUnopenedPacks),
	"Referrals":              postgres.Query(getReferrals),
	"Purchase":               postgres.Query(getPurchase),
	"Purchases":              postgres.Query(getPurchases),
	"UserPurchases":          postgres.Query(getUserPurchases),
	"UnclaimedUserPurchases": postgres.Query(getUnclaimedUserPurchases),
	"User":          postgres.Query(getUser),
	"GetUserByName": postgres.Query(getUserByName),
	"CreateUser":    postgres.Query(createUser),
	"OpenPack":      postgres.Query(openPack),

	// auctions
	"Bids":     postgres.Query(getBids),
	"Auctions": postgres.Query(getAuctions),
	"Auction":  postgres.Query(getAuction),
}
