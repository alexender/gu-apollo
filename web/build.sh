# xgo –targets=linux/amd64
rm -rf bin
rm -rf upload.zip
# path must be to this directory hence: ./
# linux/amd as this is what our AWS server runs
#xgo -out bin/application --targets=linux/amd64 --deps=https://gmplib.org/download/gmp/gmp-6.1.0.tar.bz2 ./
env GOOS=linux GOARCH=amd64 go build -o bin/application *.go
# MUST BE CALLED APPLICATION
#cp bin/application-linux-amd64 bin/application
#rm -rf bin/application-linux-amd64
zip upload.zip -r bin/application