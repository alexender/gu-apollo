package server

import (
	"fuel/apollo/apollo"
	"net/http"

	"github.com/gorilla/mux"
)

func getPurchase(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	id, e := getID("id", r)

	if e != nil {
		return nil, e
	}

	query := apollo.Query{
		Name: "Purchase",
		Params: map[string]interface{}{
			"id": id,
		},
	}
	json, err := client.Datastore.Query(query)
	if err != nil {
		return nil, &handlerError{err, "get from datastore failed" + err.Error(), http.StatusInternalServerError}
	}

	return json, nil
}

func getPurchases(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	query := apollo.Query{
		Name: "Purchases",
	}

	json, err := client.Datastore.Query(query)
	if err != nil {
		return nil, &handlerError{err, "get all from datastore failed" + err.Error(), http.StatusInternalServerError}
	}

	return json, nil
}

func getUserPurchases(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	address := mux.Vars(r)["address"]

	query := apollo.Query{
		Name: "UserPurchases",
		Params: map[string]interface{}{
			"address": address,
		},
	}

	json, err := client.Datastore.Query(query)
	if err != nil {
		return nil, &handlerError{err, "get all from datastore failed", http.StatusInternalServerError}
	}

	return json, nil
}

func getUnclaimedUserPurchases(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	address := mux.Vars(r)["address"]

	query := apollo.Query{
		Name: "UnclaimedUserPurchases",
		Params: map[string]interface{}{
			"address": address,
		},
	}

	json, err := client.Datastore.Query(query)
	if err != nil {
		return nil, &handlerError{err, "get all from datastore failed", http.StatusInternalServerError}
	}

	return json, nil
}
