package model

import (
	"database/sql"

	"github.com/lib/pq"
)

type Purchase struct {
	ID        string `db:"id" json:"id"`
	User      string `db:"user" json:"user"`
	Count     int    `db:"count" json:"count"`
	Remaining int    `db:"remaining" json:"remaining"`
	Factory   string `db:"factory" json:"factory"`
	EventID   int    `db:"eventid" json:"-"`
}

type Pack struct {
	QueryID      string        `db:"queryid" json:"queryid"`
	QueryIndex   int           `db:"queryindex" json:"queryindex"`
	QueryIndices pq.Int64Array `db:"queryindices" json:"queryindices"`
	User         string        `db:"user" json:"user"`
	Factory      string        `db:"factory" json:"factory"`
	EventID      int           `db:"eventid" json:"-"`
	Opened       bool          `db:"opened" json:"opened"`
	Cards        []Card        `json:"cards"`
}

type Card struct {
	QueryID    string         `db:"queryid" json:"queryid"`
	QueryIndex string         `db:"queryindex" json:"queryindex"`
	ID         sql.NullInt64  `db:"id" json:"id"`
	Proto      int            `db:"proto" json:"proto"`
	Purity     int            `db:"purity" json:"purity"`
	User       string         `db:"user" json:"user"`
	Previous   sql.NullString `db:"previous" json:"-"`
	Opened     bool           `db:"opened" json:"opened"`
	EventID    int            `db:"eventid" json:"-"`
}

type Referral struct {
	Referrer  string `db:"referrer" json:"referrer"`
	Purchaser string `db:"purchaser" json:"purchaser"`
	Factory   string `db:"factory" json:"factory"`
	Value     int    `db:"value" json:"value"`
	EventID   int    `db:"eventid" json:"-"`
}

type User struct {
	Username string        `db:"username" json:"username"`
	Email    string        `db:"email" json:"email"`
	Address  string        `db:"address" json:"address"`
	Showcase pq.Int64Array `db:"showcase" json:"showcase"`
	Nonce    int           `db:"nonce" json:"nonce"`
}

type Auction struct {
	ID            uint64 `db:"id" json:"id"`
	Proto         int    `db:"proto" json:"proto"`
	Purity        int    `db:"purity" json:"purity"`
	HighestBidder string `db:"highestbidder" json:"highest_bidder"`
	HighestBid    uint64 `db:"highestbid" json:"highest_bid"`
	EventID       int    `db:"eventid" json:"-"`
	Start         uint64 `db:"start" json:"start"`
	Length        uint64 `db:"length" json:"length"`
	Claimed       bool   `db:"claimed" json:"claimed"`
	Open          bool   `db:"open" json:"open"`
}

type Bid struct {
	AuctionID uint64 `db:"auctionid" json:"auction_id"`
	Bidder    string `db:"bidder" json:"bidder"`
	Value     uint64 `db:"value" json:"value"`
}
