package server

import (
	"encoding/json"
	"fmt"
	"fuel/apollo/apollo"
	"io/ioutil"
	"net/http"
	"strconv"
)

func getVaultBalance(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	url := `https://api-rinkeby.etherscan.io/api?module=account&action=balance&address=%s&tag=latest&apiKey=%s`

	vault := `0x4f00f3b9e2d63771108fe12e57960d87dade0f8f`
	apikey := `PZU9U6XE3DQJAHAA578WNFRHMES5J7AHTV`

	full := fmt.Sprintf(url, vault, apikey)

	res, err := http.Get(full)
	if err != nil {
		return nil, &handlerError{err, "", http.StatusInternalServerError}
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	var data struct {
		Status  string
		Message string
		Result  string
	}

	err = json.Unmarshal(body, &data)
	if err != nil {
		return nil, &handlerError{err, "error unmarshalling vault", http.StatusInternalServerError}
	}

	balance, err := strconv.ParseInt(data.Result, 10, 64)

	if err != nil {
		return nil, &handlerError{err, "error converting balance", http.StatusInternalServerError}
	}

	return balance, nil
}
