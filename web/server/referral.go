package server

import (
	"fuel/apollo/apollo"
	"net/http"

	"github.com/gorilla/mux"
)

func getReferrals(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	address := mux.Vars(r)["address"]

	query := apollo.Query{
		Name: "Referrals",
		Params: map[string]interface{}{
			"address": address,
		},
	}

	data, err := client.Datastore.Query(query)
	if err != nil {
		return nil, &handlerError{err, "get all referrals from datastore failed", http.StatusInternalServerError}
	}

	return data, nil
}
