package server

import (
	"encoding/json"
	"fmt"
	"fuel/apollo/apollo"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// event prefix: blockchain event
// state prefix:
// tx prefix:
// external prefix:

func Start(client *apollo.Client) {

	log.Println("RUNNING SERVER")

	r := mux.NewRouter()

	http.Handle("/", r)

	r.Handle("/proto/{id}", handler{client, getProto}).Methods("GET")

	r.Handle("/card/{id}", handler{client, getCard}).Methods("GET")
	r.Handle("/card", handler{client, getCards}).Methods("GET")

	r.Handle("/pack/{id}", handler{client, getPack}).Methods("GET")
	r.Handle("/pack", handler{client, getPacks}).Methods("GET")
	r.Handle("/query/{id}/pack/{index}/open", handler{client, openPack})

	r.Handle("/purchase/{id}", handler{client, getPurchase}).Methods("GET")
	r.Handle("/purchase", handler{client, getPurchases}).Methods("GET")

	r.Handle("/user/{address}/card", handler{client, getUserCards}).Methods("GET")
	r.Handle("/user/{address}/card/opened", handler{client, getUserOpenedCards}).Methods("GET")
	r.Handle("/user/{address}/purchase", handler{client, getUserPurchases}).Methods("GET")
	r.Handle("/user/{address}/purchase/unclaimed", handler{client, getUnclaimedUserPurchases}).Methods("GET")
	r.Handle("/user/{address}/pack", handler{client, getUserPacks}).Methods("GET")
	r.Handle("/user/{address}/pack/unopened", handler{client, getUnopenedPacks}).Methods("GET")
	r.Handle("/user/{address}/referral", handler{client, getReferrals}).Methods("GET")

	//r.Handle("/user", handler{client, getUsers}).Methods("GET")

	r.Handle("/user/authenticate", handler{client, authenticate}).Methods("POST")
	r.Handle("/user", handler{client, createUser}).Methods("POST")
	r.Handle("/user/{address}", authenticatedHandler{client, updateUser}).Methods("PUT")

	r.Handle("/user/{address}", handler{client, getUser}).Methods("GET")
	r.Handle("/user/name/{name}", handler{client, getUserByName}).Methods("GET")

	r.Handle("/auction/{id}", handler{client, getAuction}).Methods("GET")
	r.Handle("/auction", handler{client, getAuctions}).Methods("GET")
	r.Handle("/auction/{id}/bid", handler{client, getBids}).Methods("GET")

	r.Handle("/vault/balance", handler{client, getVaultBalance}).Methods("GET")

	r.Handle("/", handler{client, func(c *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
		return "This is a working api.", nil
	}})

	log.Fatal(http.ListenAndServe(":5000", nil))
}

type authenticatedHandler struct {
	client *apollo.Client
	fn     func(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError)
}

type handler struct {
	client *apollo.Client
	fn     func(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError)
}

type handlerError struct {
	Error   error
	Message string
	Code    int
}

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	// call the handler
	response, err := h.fn(h.client, w, r)

	basicServe(response, err, w, r)
}

func basicServe(response interface{}, err *handlerError, w http.ResponseWriter, r *http.Request) {
	if err != nil {
		log.Printf("ERROR: %v\n", err.Error)
		http.Error(w, fmt.Sprintf(`{"error":"%s}`, err.Message), err.Code)
		return
	}

	if response == nil {
		log.Println("ERROR: response from method is nil")
		http.Error(w, "Internal server error. Check the logs.", http.StatusInternalServerError)
		return
	}

	bytes, e := json.Marshal(response)
	if e != nil {
		log.Println(response)
		http.Error(w, "Error marshalling JSON", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(bytes)
	log.Printf("%s %s %s %d", r.RemoteAddr, r.Method, r.URL, 200)
}

func getID(name string, r *http.Request) (int64, *handlerError) {
	param := mux.Vars(r)[name]
	id, err := strconv.ParseInt(param, 10, 64)
	if err != nil {
		return 0, &handlerError{err, fmt.Sprintf("Failed to convert parameter: %s", name), http.StatusNotFound}
	}
	return id, nil
}

func getIDs(r *http.Request, names ...string) (ids []int64, e *handlerError) {
	for _, n := range names {
		id, err := getID(n, r)
		if err != nil {
			return nil, err
		}
		ids = append(ids, id)
	}
	return ids, nil
}
