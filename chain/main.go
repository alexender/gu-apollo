package main

import (
	"fuel/apollo/apollo"
	"fuel/apollo/geth"
	"log"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
)

const (
	shiny     = "0x031212fc9ee565babec50a190875ad3b259d26bd"
	legendary = "0xc4e5c165ade9e22258d1fc7fc99d59bf83028e08"
	epic      = "0xf57e50e60b5a5dc32066626712d1da101741c5b8"
	rare      = "0xf6f86aaa7aef4f252260bee3ffd83bb8f0f69a6a"

	auction = "0x7dd4bd6c0f10c869e55062575cda7add971b3327"

	core = "0x4cc1aa45a3a9696f4da44c2ac814ed0e63e26d0a"

	signatures = "0x126624becb8ca16d094b80bcb5ca5252cd162340"
)

var (
	// use the ABI from the base pack contract
	packABI    abi.ABI
	coreABI    abi.ABI
	auctionABI abi.ABI
)

func main() {
	var err error

	coreABI, err = abi.JSON(strings.NewReader(coreABIString))

	if err != nil {
		log.Fatal(err)
	}

	packABI, err = abi.JSON(strings.NewReader(packABIString))

	if err != nil {
		log.Fatal(err)
	}

	auctionABI, err = abi.JSON(strings.NewReader(auctionABIString))

	if err != nil {
		log.Fatal(err)
	}

	config := geth.Config{
		ConsumerLookups: []string{"127.0.0.1:4151"},
		ConsumerTopic:   "Apollo-Callback",
		ConsumerChannel: "geth",
		ProducerAddress: "127.0.0.1:4150",
		ProducerTopic:   "Apollo",
	}

	client, err := ethclient.Dial("ws://localhost:8546")

	if err != nil {
		log.Fatal(err)
	}

	listener := geth.NewListener(&config, client, geth.MakeQueryFunc(processLog))

	core := []common.Address{common.HexToAddress(core)}

	packs := []common.Address{
		common.HexToAddress(shiny),
		common.HexToAddress(legendary),
		common.HexToAddress(epic),
		common.HexToAddress(rare),
	}

	fqs := []ethereum.FilterQuery{
		ethereum.FilterQuery{
			Topics:    [][]common.Hash{[]common.Hash{coreABI.Events["Transfer"].Id()}},
			Addresses: core,
		},
		/*ethereum.FilterQuery{
			Topics:    [][]common.Hash{[]common.Hash{coreABI.Events["CardCreated"].Id()}},
			Addresses: core,
		},*/
		ethereum.FilterQuery{
			Topics:    [][]common.Hash{[]common.Hash{packABI.Events["PackOpened"].Id()}},
			Addresses: packs,
			FromBlock: big.NewInt(5933453),
		},
		ethereum.FilterQuery{
			Topics:    [][]common.Hash{[]common.Hash{packABI.Events["Referral"].Id()}},
			Addresses: packs,
			FromBlock: big.NewInt(5933453),
		},
		ethereum.FilterQuery{
			Topics:    [][]common.Hash{[]common.Hash{packABI.Events["PacksPurchased"].Id()}},
			Addresses: packs,
			FromBlock: big.NewInt(5933453),
		},
		ethereum.FilterQuery{
			Topics:    [][]common.Hash{[]common.Hash{packABI.Events["RandomnessReceived"].Id()}},
			Addresses: packs,
			FromBlock: big.NewInt(5933453),
		},
		ethereum.FilterQuery{
			Topics: [][]common.Hash{
				[]common.Hash{auctionABI.Events["Claimed"].Id()},
				[]common.Hash{auctionABI.Events["Extended"].Id()},
				[]common.Hash{auctionABI.Events["Opened"].Id()},
				[]common.Hash{auctionABI.Events["Bid"].Id()},
				[]common.Hash{auctionABI.Events["Created"].Id()},
			},
			Addresses: []common.Address{common.HexToAddress(auction)},
		},
	}

	listener.Run(fqs)

}

/*var logMap = map[string]geth.MakeQueryFunc{
	// core events
	coreABI.Events["CardCreated"].Id().Hex(): processCardCreated,
	coreABI.Events["Transfer"].Id().Hex():    processTransfer,
	// pack events
	packABI.Events["PackOpened"].Id().Hex():         processPackOpened,
	packABI.Events["PacksPurchased"].Id().Hex():     processPurchase,
	packABI.Events["Referral"].Id().Hex():           processReferral,
	packABI.Events["RandomnessReceived"].Id().Hex(): processRandomness,
	// auction events
	auctionABI.Events["Claimed"].Id().Hex():  processAuctionClaimed,
	auctionABI.Events["Bid"].Id().Hex():      processAuctionBid,
	auctionABI.Events["Created"].Id().Hex():  processAuctionCreated,
	auctionABI.Events["Extended"].Id().Hex(): processAuctionExtended,
	auctionABI.Events["Created"].Id().Hex():  processAuctionCreated,
}*/

func processLog(l *geth.Listener, lg types.Log) *apollo.Query {

	log.Println("processing event")

	// TODO: this doesn't work as a map
	switch lg.Topics[0] {
	/*case coreABI.Events["CardCreated"].Id():
	log.Println("cardcreated")
	return processCardCreated(l, lg)*/
	case coreABI.Events["Transfer"].Id():
		log.Println("transfer")
		return processTransfer(l, lg)
	// pack events
	case packABI.Events["PackOpened"].Id():
		log.Println("opened")
		return processPackOpened(l, lg)
	case packABI.Events["PacksPurchased"].Id():
		log.Println("purchase")
		return processPurchase(l, lg)
	case packABI.Events["Referral"].Id():
		log.Println("referral")
		return processReferral(l, lg)
	case packABI.Events["RandomnessReceived"].Id():
		log.Println("randomness received")
		return processRandomness(l, lg)
	// auction events
	case auctionABI.Events["Claimed"].Id():
		log.Println("claimed")
		return processAuctionClaimed(l, lg)
	case auctionABI.Events["Bid"].Id():
		log.Println("bid")
		return processAuctionBid(l, lg)
	case auctionABI.Events["Created"].Id():
		return processAuctionCreated(l, lg)
	case auctionABI.Events["Extended"].Id():
		return processAuctionExtended(l, lg)
	case auctionABI.Events["Created"].Id():
		return processAuctionCreated(l, lg)
	}
	log.Println("event sig not recognised")
	return nil
}

func asHexAddress(hash common.Hash) string {
	return common.BytesToAddress(hash.Bytes()).Hex()
}

func createParams(lg types.Log) map[string]interface{} {
	return map[string]interface{}{
		"txhash":      lg.TxHash.Hex(),
		"index":       uint64(lg.Index),
		"removed":     lg.Removed,
		"blockhash":   lg.BlockHash.Hex(),
		"blocknumber": lg.BlockNumber,
		"address":     lg.Address.Hex(),
	}
}

func processPurchase(l *geth.Listener, lg types.Log) *apollo.Query {

	params := createParams(lg)

	var data struct {
		Count uint64
	}

	err := packABI.Unpack(&data, "PacksPurchased", lg.Data)

	if err != nil {
		log.Println(err)
	}

	params["id"] = lg.Topics[1].Hex()
	params["user"] = asHexAddress(lg.Topics[2])
	params["count"] = uint64(data.Count)

	return &apollo.Query{
		Name:   "PacksPurchased",
		Params: params,
	}
}

func processReferral(l *geth.Listener, lg types.Log) *apollo.Query {

	log.Println("processing referral")

	params := createParams(lg)

	var data struct {
		Purchaser common.Address
		Value     *big.Int
	}

	err := packABI.Unpack(&data, "Referral", lg.Data)

	if err != nil {
		log.Println(err)
	}

	params["referrer"] = asHexAddress(lg.Topics[1])
	params["purchaser"] = data.Purchaser.Hex()
	params["value"] = data.Value.Uint64()

	return &apollo.Query{
		Name:   "Referral",
		Params: params,
	}
}

func processPackOpened(l *geth.Listener, lg types.Log) *apollo.Query {

	log.Println("processing pack opened")

	params := createParams(lg)
	var data struct {
		StartIndex uint64
		CardIDs    []*big.Int
	}

	err := packABI.Unpack(&data, "PackOpened", lg.Data)

	if err != nil {
		log.Println(err)
		return nil
	}

	var ids []uint64

	for _, id := range data.CardIDs {
		ids = append(ids, id.Uint64())
	}

	params["queryid"] = lg.Topics[1].Hex()
	params["startindex"] = data.StartIndex
	params["user"] = asHexAddress(lg.Topics[2])
	params["ids"] = ids

	return &apollo.Query{
		Name:   "PackCreated",
		Params: params,
	}
}

func processCardCreated(l *geth.Listener, lg types.Log) *apollo.Query {

	params := createParams(lg)

	var data struct {
		Owner  common.Address
		Purity uint16
	}

	err := coreABI.Unpack(&data, "CardCreated", lg.Data)

	if err != nil {
		log.Println(err)
		return nil
	}

	params["id"] = lg.Topics[1].Big().Uint64()
	params["proto"] = lg.Topics[2].Big().Uint64()
	params["purity"] = data.Purity
	params["user"] = data.Owner.Hex()

	return &apollo.Query{
		Name:   "CardCreated",
		Params: params,
	}
}

func processTransfer(l *geth.Listener, lg types.Log) *apollo.Query {

	params := createParams(lg)

	var data struct {
		TokenId *big.Int
	}

	err := coreABI.Unpack(&data, "Transfer", lg.Data)

	if err != nil {
		log.Println(err)
		return nil
	}

	params["from"] = asHexAddress(lg.Topics[1])
	params["to"] = asHexAddress(lg.Topics[2])

	params["id"] = data.TokenId.Uint64()

	return &apollo.Query{
		Name:   "Transfer",
		Params: params,
	}

}

func processRandomness(l *geth.Listener, lg types.Log) *apollo.Query {

	params := createParams(lg)

	var data struct {
		Count      uint64
		Randomness *big.Int
	}

	err := packABI.Unpack(&data, "RandomnessReceived", lg.Data)

	if err != nil {
		log.Println("error unpacking event", err)
		return nil
	}

	params["id"] = lg.Topics[1].Hex()

	log.Println(params["id"])

	params["user"] = asHexAddress(lg.Topics[2])

	var arr [32]byte

	copy(arr[:], lg.Topics[1].Bytes())

	bytes, err := packABI.Pack("predictPacks", arr)

	if err != nil {
		log.Println("error packing args", err)
		return nil
	}

	addr := lg.Address

	msg := ethereum.CallMsg{
		To:   &addr,
		Data: bytes,
	}

	result, err := l.Call(msg)

	if err != nil {
		log.Println("error making call", err)
		return nil
	}

	var cardData struct {
		Protos   []uint16
		Purities []uint16
	}

	err = packABI.Unpack(&cardData, "predictPacks", result)

	if err != nil {
		log.Println("error unpacking call result", err)
		return nil
	}

	params["protos"] = cardData.Protos
	params["purities"] = cardData.Purities

	return &apollo.Query{
		Name:   "RandomnessReceived",
		Params: params,
	}

}

func processAuctionClaimed(l *geth.Listener, lg types.Log) *apollo.Query {

	params := createParams(lg)

	return &apollo.Query{
		Name:   "AuctionClaimed",
		Params: params,
	}

}

func processAuctionCreated(l *geth.Listener, lg types.Log) *apollo.Query {

	params := createParams(lg)

	return &apollo.Query{
		Name:   "AuctionCreated",
		Params: params,
	}

}

func processAuctionBid(l *geth.Listener, lg types.Log) *apollo.Query {

	params := createParams(lg)

	var data struct {
		Bidder string
		Value  uint64
	}

	err := auctionABI.Unpack(&data, "AuctionBid", lg.Data)

	if err != nil {
		log.Println(err)
		return nil
	}

	params["value"] = data.Value
	params["bidder"] = data.Bidder
	params["id"] = lg.Topics[0].Big().Uint64()

	return &apollo.Query{
		Name:   "AuctionBid",
		Params: params,
	}

}

func processAuctionOpened(l *geth.Listener, lg types.Log) *apollo.Query {

	params := createParams(lg)

	var data struct {
		Start uint64
	}

	err := auctionABI.Unpack(&data, "AuctionOpened", lg.Data)

	if err != nil {
		log.Println(err)
		return nil
	}

	params["start"] = data.Start
	params["id"] = lg.Topics[0].Big().Uint64()

	return &apollo.Query{
		Name:   "AuctionOpened",
		Params: params,
	}

}

func processAuctionExtended(l *geth.Listener, lg types.Log) *apollo.Query {

	params := createParams(lg)

	var data struct {
		Length uint64
	}

	err := auctionABI.Unpack(&data, "AuctionExtended", lg.Data)

	if err != nil {
		log.Println(err)
		return nil
	}

	params["length"] = data.Length
	params["id"] = lg.Topics[0].Big().Uint64()

	return &apollo.Query{
		Name:   "AuctionExtended",
		Params: params,
	}

}
