package main

import (
	"fuel/apollo/geth"
	"log"
	"strings"
	"testing"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
)

func TestCall(t *testing.T) {

	log.Println("hi")

	var err error

	coreABI, err = abi.JSON(strings.NewReader(coreABIString))

	if err != nil {
		t.FailNow()
	}

	packABI, err = abi.JSON(strings.NewReader(packABIString))

	if err != nil {
		t.FailNow()
	}

	config := geth.Config{
		ConsumerLookups: []string{"127.0.0.1:4151"},
		ConsumerTopic:   "Apollo-Callback",
		ConsumerChannel: "geth",
		ProducerAddress: "127.0.0.1:4150",
		ProducerTopic:   "Apollo",
	}

	client, err := ethclient.Dial("ws://localhost:8546")

	if err != nil {
		log.Println("hereeeee")
		t.Fatalf(err.Error())
	}

	listener := geth.NewListener(&config, client, geth.MakeQueryFunc(processLog))

	var arr [32]byte

	id := common.FromHex("0xa00a15ceaadae5b671a8ce86ecf02da3fea1719a31047d5eae8e3b76da9dff49")

	if len(id) != 32 {
		log.Println(id)
		t.Fatalf("length is not 32: %d", len(id))
	}

	copy(arr[:], id)

	//bytes, err := packABI.Pack("predictPacks", arr)
	bytes, err := packABI.Pack("predictPacks", arr)

	log.Println(bytes)

	if err != nil {
		t.FailNow()
	}

	addr := common.HexToAddress(rare)

	msg := ethereum.CallMsg{
		To:   &addr,
		Data: bytes,
	}

	result, err := listener.Call(msg)

	if err != nil {
		t.FailNow()
	}

	log.Println("result", result)

	var cardData struct {
		Protos   []uint16
		Purities []uint16
	}

	err = packABI.Unpack(&cardData, "predictPacks", result)

	if err != nil {
		t.FailNow()
	}

	log.Println("cardData", cardData)

	t.Fail()
}
