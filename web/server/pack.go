package server

import (
	"errors"
	"fuel/apollo/apollo"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func getUserPacks(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	address := mux.Vars(r)["address"]

	if address == "" {
		return nil, &handlerError{errors.New("no address provided"), "get packs failed", http.StatusBadRequest}
	}

	query := apollo.Query{
		Name: "UserPacks",
		Params: map[string]interface{}{
			"address": address,
		},
	}
	json, err := client.Datastore.Query(query)
	if err != nil {
		return nil, &handlerError{err, "get all from datastore failed", http.StatusInternalServerError}
	}

	return json, nil
}

func getUnopenedPacks(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	address := mux.Vars(r)["address"]

	if address == "" {
		return nil, &handlerError{errors.New("no address provided"), "get packs failed", http.StatusBadRequest}
	}

	query := apollo.Query{
		Name: "UnopenedPacks",
		Params: map[string]interface{}{
			"address": address,
		},
	}
	json, err := client.Datastore.Query(query)
	if err != nil {
		return nil, &handlerError{err, "get all from datastore failed", http.StatusInternalServerError}
	}

	return json, nil
}

func getPack(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	id, e := getID("id", r)

	if e != nil {
		return nil, e
	}

	query := apollo.Query{
		Name: "Pack",
		Params: map[string]interface{}{
			"id": id,
		},
	}
	json, err := client.Datastore.Query(query)

	if err != nil {
		return nil, &handlerError{err, "get from datastore failed", http.StatusInternalServerError}
	}

	return json, nil
}

func getPacks(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	log.Println("getting packs")

	query := apollo.Query{
		Name: "Packs",
	}
	data, err := client.Datastore.Query(query)
	if err != nil {
		return nil, &handlerError{err, "get all from datastore failed", http.StatusInternalServerError}
	}

	return data, nil
}

func openPack(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	id := mux.Vars(r)["id"]

	index, e := getID("index", r)

	if e != nil {
		return nil, e
	}

	query := apollo.Query{
		Name: "OpenPack",
		Params: map[string]interface{}{
			"id":    id,
			"index": index,
		},
	}

	data, err := client.Datastore.Query(query)

	if err != nil {
		return nil, &handlerError{err, "get from datastore failed", http.StatusInternalServerError}
	}

	return data, nil
}
