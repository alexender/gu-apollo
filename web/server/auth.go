package server

import (
	"demos/ansible/btcd/btcec"
	"encoding/json"
	"fmt"
	"fuel/apollo/apollo"
	"fuel/gu-apollo/web/model"
	"io/ioutil"
	"log"
	"math/big"
	"net/http"
	"strconv"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
)

var (
	hmacSampleSecret = "super spooky secret message 26"
)

type AuthRequest struct {
	Pubkey  string `json:"pubkey`
	Address string `json:"address"`
	Data    string `json:"data"`
}

func decodeAuthRequest(r *http.Request) AuthRequest {
	var u AuthRequest
	body, _ := ioutil.ReadAll(r.Body)
	if err := json.Unmarshal(body, &u); err != nil {
		log.Println("Could not decode body:", err.Error())
	}
	defer r.Body.Close()
	return u
}

func authenticate(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	req := decodeAuthRequest(r)

	data, err := client.Datastore.Query(apollo.Query{
		Name: "User",
		Params: map[string]interface{}{
			"address": req.Address,
		},
	})

	user := data.(model.User)

	if err != nil {
		return nil, &handlerError{err, "get from datastore failed", http.StatusInternalServerError}
	}

	provided := []byte(req.Data)
	nonce := strconv.Itoa(user.Nonce)
	expected := []byte("GODS UNCHAINED AUTHENTICATION " + nonce)

	// don't do this on server
	// do it on client
	// the geth one uses c-go, cross-compiling is an issue
	// and i don't trust the other ones
	pubKey := []byte(req.Pubkey)

	if ok := VerifySignature(pubKey, provided, expected); !ok {
		return nil, &handlerError{err, "signature verification failed", http.StatusForbidden}
	}

	// TODO: verify the signature on the data they send

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"address": req.Address,
	})

	tokenString, err := token.SignedString(hmacSampleSecret)

	client.Datastore.Query(apollo.Query{
		Name: "IncrementNonce",
		Params: map[string]interface{}{
			"address": req.Address,
		},
	})

	return tokenString, nil
}

func (h authenticatedHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	address := mux.Vars(r)["address"]

	body, _ := ioutil.ReadAll(r.Body)

	tokenString := string(body)

	err := requireAuthentication(address, tokenString)

	if err != nil {
		basicServe(nil, &handlerError{err, "wrong permissions", http.StatusForbidden}, w, r)
		return
	}

	response, he := h.fn(h.client, w, r)

	basicServe(response, he, w, r)
}

func requireAuthentication(address string, tokenString string) error {

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return hmacSampleSecret, nil
	})

	if err != nil {
		return err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if address == claims["address"] {
			return nil
		}
		return fmt.Errorf("Invalid auth token address for api method")
	}
	return fmt.Errorf("Invalid auth token for api method")
}

/// taken from geth/crypto
// easier this way than trying to compile with xgo

var (
	secp256k1N, _  = new(big.Int).SetString("fffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141", 16)
	secp256k1halfN = new(big.Int).Div(secp256k1N, big.NewInt(2))
)

func VerifySignature(pubkey, hash, signature []byte) bool {
	if len(signature) != 64 {
		return false
	}
	sig := &btcec.Signature{R: new(big.Int).SetBytes(signature[:32]), S: new(big.Int).SetBytes(signature[32:])}
	key, err := btcec.ParsePubKey(pubkey, btcec.S256())
	if err != nil {
		return false
	}
	// Reject malleable signatures. libsecp256k1 does this check but btcec doesn't.
	if sig.S.Cmp(secp256k1halfN) > 0 {
		return false
	}
	return sig.Verify(hash, key)
}
