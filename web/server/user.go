package server

import (
	"encoding/json"
	"fuel/apollo/apollo"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type User struct {
	Address  string `json:"address"`
	Email    string `json:"email"`
	Nonce    string `json:"nonce"`
	Username string `json:"username"`
}

func getUser(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	address := mux.Vars(r)["address"]

	query := apollo.Query{
		Name: "User",
		Params: map[string]interface{}{
			"address": address,
		},
	}
	data, err := client.Datastore.Query(query)

	if err != nil {
		return nil, &handlerError{err, "get from datastore failed", http.StatusInternalServerError}
	}

	return data, nil
}

func getUserByName(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	name := mux.Vars(r)["name"]

	query := apollo.Query{
		Name: "User",
		Params: map[string]interface{}{
			"name": name,
		},
	}
	data, err := client.Datastore.Query(query)

	if err != nil {
		return nil, &handlerError{err, "get from datastore failed", http.StatusInternalServerError}
	}

	return data, nil
}

func getUsers(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	query := apollo.Query{Name: "Users"}

	data, err := client.Datastore.Query(query)

	if err != nil {
		return nil, &handlerError{err, "get from datastore failed", http.StatusInternalServerError}
	}

	return data, nil
}

func decodeUser(r *http.Request) User {
	var u User
	body, _ := ioutil.ReadAll(r.Body)
	if err := json.Unmarshal(body, &u); err != nil {
		log.Println("Could not decode body:", err.Error())
	}
	defer r.Body.Close()
	return u
}

func createUser(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	user := decodeUser(r)

	query := apollo.Query{
		Name: "CreateUser",
		Params: map[string]interface{}{
			"user": user,
		},
	}
	data, err := client.Datastore.Query(query)

	if err != nil {
		return nil, &handlerError{err, "get from datastore failed", http.StatusInternalServerError}
	}

	return data, nil
}

func updateUser(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	user := decodeUser(r)

	query := apollo.Query{
		Name: "UpdateUser",
		Params: map[string]interface{}{
			"user": user,
		},
	}
	data, err := client.Datastore.Query(query)

	if err != nil {
		return nil, &handlerError{err, "get from datastore failed", http.StatusInternalServerError}
	}

	return data, nil
}
