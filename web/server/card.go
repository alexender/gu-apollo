package server

import (
	"fuel/apollo/apollo"
	"net/http"

	"github.com/gorilla/mux"
)

func getCard(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	id, e := getID("id", r)

	if e != nil {
		return e.Error, e
	}

	query := apollo.Query{
		Name: "Card",
		Params: map[string]interface{}{
			"id": id,
		},
	}

	json, err := client.Datastore.Query(query)
	if err != nil {
		return err, &handlerError{err, "get from datastore failed" + err.Error(), http.StatusInternalServerError}
	}

	return json, nil
}

func getProto(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	id, e := getID("id", r)

	if e != nil {
		return e.Error, e
	}

	query := apollo.Query{
		Name: "Proto",
		Params: map[string]interface{}{
			"id": id,
		},
	}

	json, err := client.Datastore.Query(query)
	if err != nil {
		return err, &handlerError{err, "get from datastore failed" + err.Error(), http.StatusInternalServerError}
	}

	return json, nil
}

func getCards(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	query := apollo.Query{
		Name: "Cards",
	}

	json, err := client.Datastore.Query(query)
	if err != nil {
		return nil, &handlerError{err, "get all from datastore failed" + err.Error(), http.StatusInternalServerError}
	}

	return json, nil
}

func getUserCards(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	address := mux.Vars(r)["address"]

	query := apollo.Query{
		Name: "UserCards",
		Params: map[string]interface{}{
			"address": address,
		},
	}

	json, err := client.Datastore.Query(query)
	if err != nil {
		return nil, &handlerError{err, "get all from datastore failed", http.StatusInternalServerError}
	}

	return json, nil
}

func getUserOpenedCards(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	address := mux.Vars(r)["address"]

	query := apollo.Query{
		Name: "UserOpenedCards",
		Params: map[string]interface{}{
			"address": address,
		},
	}

	json, err := client.Datastore.Query(query)
	if err != nil {
		return nil, &handlerError{err, "get all from datastore failed", http.StatusInternalServerError}
	}

	return json, nil
}

const (
	cardABI             = ""
	cardContractAddress = ""
)
