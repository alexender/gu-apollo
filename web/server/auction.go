package server

import (
	"fuel/apollo/apollo"
	"net/http"
)

func getAuction(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	id, e := getID("id", r)

	if e != nil {
		return e.Error, e
	}

	query := apollo.Query{
		Name: "Auction",
		Params: map[string]interface{}{
			"id": id,
		},
	}

	json, err := client.Datastore.Query(query)
	if err != nil {
		return err, &handlerError{err, "get from datastore failed" + err.Error(), http.StatusInternalServerError}
	}

	return json, nil
}

func getAuctions(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	query := apollo.Query{
		Name: "Auctions",
	}

	json, err := client.Datastore.Query(query)
	if err != nil {
		return nil, &handlerError{err, "get all from datastore failed" + err.Error(), http.StatusInternalServerError}
	}

	return json, nil
}

func getBids(client *apollo.Client, w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	id, e := getID("id", r)

	if e != nil {
		return e.Error, e
	}

	query := apollo.Query{
		Name: "Bids",
		Params: map[string]interface{}{
			"id": id,
		},
	}

	json, err := client.Datastore.Query(query)
	if err != nil {
		return err, &handlerError{err, "get from datastore failed" + err.Error(), http.StatusInternalServerError}
	}

	return json, nil
}
