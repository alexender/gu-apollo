package main

import (
	"fmt"
	"fuel/apollo/apollo"
	"fuel/apollo/postgres"
	"log"
	"time"

	"github.com/jmoiron/sqlx"
)

const (
	host = "gu-apollo-db.cegfyga2tbo9.us-west-2.rds.amazonaws.com"
	port = 5432
	user = "guadmin"
	pw   = "treesandboxrabbit"
	name = "gu_apollo_db"
)

var (
	db *sqlx.DB
)

type Event struct {
	Address     string `db:"address"`
	ID          int64  `db:"id"`
	Index       int64  `db:"index"`
	Txhash      string `db:"txhash"`
	Blockhash   string `db:"blockhash"`
	Removed     bool   `db:"removed"`
	Blocknumber int64  `db:"blocknumber"`
}

func main() {

	// can use env variables
	// os.Getenv("FOO")
	// leave in there for now

	config := &postgres.Config{
		ConsumerTopic:   "Apollo",
		ConsumerChannel: "postgres",
		Lookups:         []string{"ec2-34-221-58-36.us-west-2.compute.amazonaws.com:4150"},
	}

	//ec2-34-221-58-36.us-west-2.compute.amazonaws.com

	var err error
	db, err = postgres.CreateDB(host, port, user, pw, name)

	db.SetMaxIdleConns(0)                  //No Idle connection
	db.SetMaxOpenConns(400)                //Max connection to 400
	db.SetConnMaxLifetime(time.Second * 5) //Connection dies after 5 seconds

	if err != nil {
		log.Fatal(err)
	}

	consumer := postgres.NewConsumer(config, setters)

	consumer.Run()
}

func transfer(query apollo.Query) error {

	log.Println("transfer")

	id, err := query.JsonUint64("id")
	if err != nil {
		return err
	}

	to, err := query.String("to")
	if err != nil {
		return err
	}

	from, err := query.String("from")
	if err != nil {
		return err
	}

	event, err := addEvent(query)
	if err != nil {
		return err
	}

	if event.Removed {
		// only remember the last one: hopefully not necessary to do more
		// method if needed: make a new Transfer table, store each event with its parent id, go back up 1
		// a lot of unnecessary storage --> could prune after depth n?
		_, err := db.Exec(`UPDATE card SET card.user = card.previous, card.previous = NULL, card.eventid = $1 FROM event WHERE card.id = $2 AND card.eventid = event.id AND event.blockhash = $3`, event.ID, id, event.Blockhash)

		if err != nil {
			return err
		}
	} else {
		if from != "0x0000000000000000000000000000000000000000" {
			_, err = db.Exec(`UPDATE card SET "user" = $1, previous = $2, eventid = $3, opened = true WHERE "id" = $4;`, to, from, event.ID, id)
			if err != nil {
				return err
			}

		} else {
			_, err := db.Exec(`UPDATE card SET "user" = $1, previous = $2, eventid = $3 WHERE "id" = $4;`, to, from, event.ID, id)
			if err != nil {
				return err
			}
		}

	}

	return err
}

func addEvent(query apollo.Query) (*Event, error) {

	blockhash, err := query.String("blockhash")
	if err != nil {
		return nil, err
	}

	txhash, err := query.String("txhash")
	if err != nil {
		return nil, err
	}

	index, err := query.JsonUint64("index")
	if err != nil {
		return nil, err
	}

	address, err := query.String("address")
	if err != nil {
		return nil, err
	}

	removed, err := query.Bool("removed")
	if err != nil {
		return nil, err
	}

	blocknumber, err := query.JsonUint64("blocknumber")
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(`INSERT INTO event (address, blocknumber, blockhash, txhash, index, removed) VALUES ($1, $2, $3, $4, $5, $6);`, address, blocknumber, blockhash, txhash, index, removed)

	if err != nil {
		log.Println("insert failed")
		return nil, err
	}

	var event Event

	err = db.Get(&event, `SELECT * FROM event WHERE blockhash = $1 AND txhash = $2 AND index = $3`, blockhash, txhash, index)

	if err != nil {
		return nil, err
	}

	return &event, err
}

func referral(query apollo.Query) error {

	log.Println("referral")

	value, err := query.JsonUint64("value")
	if err != nil {
		return err
	}

	purchaser, err := query.String("purchaser")
	if err != nil {
		return err
	}

	referrer, err := query.String("referrer")
	if err != nil {
		return err
	}

	event, err := addEvent(query)
	if err != nil {
		return err
	}

	if event.Removed {
		_, err := db.Exec(`REMOVE FROM referral USING event WHERE referral.eventid = event.id AND event.blockhash = $1 AND event.txhash = $2 AND event.index = #3`,
			event.Blockhash, event.Txhash, event.Index)

		if err != nil {
			return err
		}

	} else {
		_, err := db.Exec(`INSERT INTO referral (purchaser, referrer, value, eventid, factory) VALUES($1, $2, $3, $4, $5);`, purchaser, referrer, value, event.ID, event.Address)

		if err != nil {
			return err
		}

		db.Close()
	}

	return nil
}

func pack(query apollo.Query) error {

	log.Println("pack")

	queryid, err := query.String("queryid")
	if err != nil {
		return err
	}

	startIndex, err := query.JsonUint64("startindex")
	if err != nil {
		return err
	}

	ids, err := query.JsonUint64Array("ids")
	if err != nil {
		return err
	}

	/*user, err := query.String("user")
	if err != nil {
		return err
	}*/

	event, err := addEvent(query)
	if err != nil {
		return err
	}

	if event.Removed {

		/*query, args, err := sqlx.In(`UPDATE card USING event SET id = NULL WHERE card.eventid = event.id AND event.blockhash = $2id IN (?);`, ids)

		if err != nil {
			return err
		}

		query = db.Rebind(query)
		_, err = db.Query(query, args...)
		if err != nil {
			return err
		}*/
	} else {
		//_, err = db.Query(`INSERT INTO pack (id, ids, "user", factory, eventid, opened) VALUES($1, $2, $3, $4, $5, false);`, ids[0], pq.Array(ids), user, event.Address, event.ID)
		for i, id := range ids {
			_, err = db.Exec(`UPDATE card SET id = $1, eventid = $2 WHERE queryid = $3 AND queryindex = $4`, id, event.ID, queryid, startIndex+uint64(i))

			if err != nil {
				return err
			}
		}

		_, err := db.Exec(`UPDATE purchase SET remaining = remaining - $1, WHERE queryid = $2`, len(ids), queryid)

		if err != nil {
			return err
		}
	}

	return nil
}

func packsPurchased(query apollo.Query) error {

	log.Println("packs purchased")

	id, err := query.String("id")
	if err != nil {
		return err
	}

	user, err := query.String("user")
	if err != nil {
		return err
	}

	count, err := query.JsonUint64("count")
	if err != nil {
		return err
	}

	event, err := addEvent(query)
	if err != nil {
		return err
	}

	if event.Removed {
		_, err := db.Exec(`DELETE FROM purchase USING event WHERE purchase.id = $1 AND event.id = purchase.eventid AND event.blockhash = $2`, id, event.Blockhash)

		if err != nil {
			return err
		}
	} else {
		_, err := db.Exec(`INSERT INTO purchase ("user", id, count, remaining, factory, eventid) VALUES ($1, $2, $3, $4, $5, $6);`, user, id, count, count, event.Address, event.ID)

		if err != nil {
			return err
		}
	}

	if err != nil {
		return err
	}

	return nil
}

func card(query apollo.Query) error {

	id, err := query.JsonUint64("id")
	if err != nil {
		return err
	}

	proto, err := query.JsonUint64("proto")
	if err != nil {
		return err
	}

	purity, err := query.JsonUint64("purity")
	if err != nil {
		return err
	}

	user, err := query.String("user")
	if err != nil {
		return err
	}

	log.Println(id, proto, purity, user)

	/*event, err := addEvent(query)
	if err != nil {
		return err
	}

	if event.Removed {
		_, err = db.Query(`UPDATE card SET id = NULL WHERE id = $1`, id)
	} else {
		_, err = db.Query(`INSERT INTO card (id, proto, purity, eventid, "user", opened) VALUES ($1, $2, $3, $4, $5, false)`, id, proto, purity, event.ID, user)
	}*/

	return err
}

func randomnessReceived(query apollo.Query) error {

	log.Println("randomness")

	id, err := query.String("id")
	if err != nil {
		return err
	}

	user, err := query.String("user")
	if err != nil {
		return err
	}

	purities, err := query.JsonUint16Array("purities")
	if err != nil {
		return err
	}

	protos, err := query.JsonUint16Array("protos")
	if err != nil {
		return err
	}

	event, err := addEvent(query)
	if err != nil {
		return err
	}

	queryString := `INSERT INTO card (queryId, queryIndex, proto, purity, "user", opened, eventid) VALUES`

	for i, purity := range purities {
		queryString += fmt.Sprintf(` ('%s', %d, %d, %d, '%s', false, %d),`, id, i, protos[i], purity, user, event.ID)
	}

	// chop the last comma
	queryString = queryString[:len(queryString)-1]

	// Do a batch insert
	_, err = db.Exec(queryString)

	if err != nil {
		log.Println("batch card insert error")
		return err
	}

	queryString = `INSERT INTO pack (queryid, queryindex, queryindices, factory, "user", opened, eventid) VALUES`

	// assume packs always have 5 cards
	for i := 0; i < len(purities); i += 5 {
		ids := fmt.Sprintf("ARRAY[%d, %d, %d, %d, %d]", i, i+1, i+2, i+3, i+4)
		queryString += fmt.Sprintf(` ('%s', %d, %s, '%s', '%s', false, %d),`, id, i, ids, event.Address, user, event.ID)
	}

	queryString = queryString[:len(queryString)-1]

	_, err = db.Exec(queryString)

	if err != nil {
		log.Println("batch pack insert error")
		return err
	}

	return nil
}

func auctionClaimed(query apollo.Query) error {

	id, err := query.String("id")
	if err != nil {
		return err
	}

	event, err := addEvent(query)
	if err != nil {
		return err
	}

	if event.Removed {
		//_, err = db.Query(`DELETE FROM purchase USING event WHERE purchase.id = $1 AND event.id = purchase.eventid AND event.blockhash = $2`, id, event.Blockhash)
	} else {
		_, err := db.Exec(`UPDATE auction SET claimed = true, eventid = $2 WHERE id = $1`, id, event.ID)

		if err != nil {
			return err
		}
	}

	return nil
}

func auctionOpened(query apollo.Query) error {

	id, err := query.JsonUint64("id")
	if err != nil {
		return err
	}

	start, err := query.JsonUint64("start")
	if err != nil {
		return err
	}

	event, err := addEvent(query)
	if err != nil {
		return err
	}

	if event.Removed {
		//_, err = db.Query(`DELETE FROM purchase USING event WHERE purchase.id = $1 AND event.id = purchase.eventid AND event.blockhash = $2`, id, event.Blockhash)
	} else {
		_, err = db.Exec(`UPDATE auction SET opened = true, start = $1, eventid = $2 WHERE id = $3`, start, id, event.ID)
	}

	if err != nil {
		return err
	}

	return nil
}

func auctionExtended(query apollo.Query) error {

	id, err := query.JsonUint64("id")
	if err != nil {
		return err
	}

	length, err := query.JsonUint64("length")
	if err != nil {
		return err
	}

	event, err := addEvent(query)
	if err != nil {
		return err
	}

	if event.Removed {
		//_, err = db.Query(`DELETE FROM purchase USING event WHERE purchase.id = $1 AND event.id = purchase.eventid AND event.blockhash = $2`, id, event.Blockhash)
	} else {
		_, err = db.Exec(`UPDATE auction SET length = $1, eventid = $2 WHERE id = $3`, length, event.ID, id)
	}

	if err != nil {
		return err
	}

	return nil
}

func auctionCreated(query apollo.Query) error {

	id, err := query.JsonUint64("id")
	if err != nil {
		return err
	}

	proto, err := query.JsonUint16("proto")
	if err != nil {
		return err
	}

	purity, err := query.JsonUint16("purity")
	if err != nil {
		return err
	}

	min, err := query.JsonUint64("min")
	if err != nil {
		return err
	}

	length, err := query.JsonUint64("length")
	if err != nil {
		return err
	}

	event, err := addEvent(query)
	if err != nil {
		return err
	}

	if event.Removed {
		//_, err = db.Query(`DELETE FROM purchase USING event WHERE purchase.id = $1 AND event.id = purchase.eventid AND event.blockhash = $2`, id, event.Blockhash)
	} else {
		_, err = db.Query(`INSERT INTO (id, proto, purity, highestBid, length, claimed, opened, eventid) auction VALUES ($1, $2, $3, $4, $5, false, false, $6)`, id, proto, purity, min, length, event.ID)
	}

	if err != nil {
		return err
	}

	return nil
}

func auctionBid(query apollo.Query) error {

	id, err := query.JsonUint64("id")
	if err != nil {
		return err
	}

	bidder, err := query.String("bidder")
	if err != nil {
		return err
	}

	value, err := query.JsonUint64("value")
	if err != nil {
		return err
	}

	event, err := addEvent(query)
	if err != nil {
		return err
	}

	if event.Removed {
		//_, err = db.Query(`DELETE FROM purchase USING event WHERE purchase.id = $1 AND event.id = purchase.eventid AND event.blockhash = $2`, id, event.Blockhash)
	} else {
		_, err = db.Query(`UPDATE Auction SET highestBidder = $1, highestBid = $2, eventid = $3 WHERE id = $4 AND $2 > highestBid`, bidder, value, event.ID, id)
		_, err = db.Query(`INSERT INTO Bid (auctionid, bidder, value) VALUES ($1, $2, $3)`, id, bidder, value)
	}

	if err != nil {
		return err
	}

	return nil
}

var setters = postgres.SetterMap{
	"Transfer":           postgres.Setter(transfer),
	"PackCreated":        postgres.Setter(pack),
	"CardCreated":        postgres.Setter(card),
	"PacksPurchased":     postgres.Setter(packsPurchased),
	"Referral":           postgres.Setter(referral),
	"RandomnessReceived": postgres.Setter(randomnessReceived),
	// auction events
	"AuctionClaimed":  postgres.Setter(auctionClaimed),
	"AuctionBid":      postgres.Setter(auctionBid),
	"AuctionOpened":   postgres.Setter(auctionOpened),
	"AuctionCreated":  postgres.Setter(auctionCreated),
	"AuctionExtended": postgres.Setter(auctionExtended),
}
